//
//  AppDelegate.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 11/4/16
//  Copyright (c) 2016 Rosberry. All rights reserved.
//

import UIKit
import Fabric
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        Fabric.with([Twitter.self])
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        window!.makeKeyAndVisible()
        window!.backgroundColor = .white
        window!.rootViewController = UINavigationController(rootViewController: LoginViewController())
        
        Routing.initialize()
    }
}
