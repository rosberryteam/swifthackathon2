//
//  TimelineService.swift
//  AaaSwift
//
//  Created by Anton Kovalev on 04.11.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class TimelineService {
    
    let builder = AuthorizationHeaderBuilder()
    
    func timeline(_ completion : @escaping ([Tweet]?, NSError?) -> ()) {
        let urlString = "https://api.twitter.com/1.1/statuses/home_timeline.json"
        let oauth = builder.build(withTwitterSession: AuthorizationService.sharedInstance.twitterSession!, forRequestMethod: "GET", urlString: urlString, parameters: nil)
        Alamofire.request(urlString, headers: oauth).responseArray { (response: DataResponse<[Tweet]>) in
            if let tweets = response.result.value {
                completion(tweets, nil)
            }
            else {
                completion(nil, NSError(domain: "AAASwift", code: -1, userInfo: nil))
            }
        }
    }
    
}
