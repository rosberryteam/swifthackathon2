//
//  AuthorizationHeadersBuilder.swift
//  AaaSwift
//
//  Created by Anton Kovalev on 04.11.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import TwitterKit

class AuthorizationHeaderBuilder {
    
    func build(withTwitterSession session: TWTRSession, forRequestMethod method: String, urlString: String, parameters: [AnyHashable : Any]?) -> [String : String] {
        let twitter = Twitter.sharedInstance()
        let oauthSigning = TWTROAuthSigning(authConfig:twitter.authConfig, authSession:session)
        let oauth = oauthSigning.oAuthEchoHeaders(forRequestMethod: method, urlString: urlString, parameters: parameters, error: nil)
        return ["Authorization":oauth["X-Verify-Credentials-Authorization"] as! String]
    }
    
}
