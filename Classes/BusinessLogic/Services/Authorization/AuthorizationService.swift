//
//  AuthorizationService.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import TwitterKit

struct Constants {
    static let sessionKey = "session"
    static let sessionUserNameKey = "username"
}

class AuthorizationService {
    static let sharedInstance = AuthorizationService()
    var session: Session?
    var twitterSession: TWTRSession?
    
    
    func logIn(completion: @escaping (Session?, Error?) -> Swift.Void) {
        Twitter.sharedInstance().logIn { session, error in
            if let session = session {
                self.session = Session(authToken:session.authToken, userID:session.userID)
                UserDefaults.standard.setValue(session.userName, forKey: Constants.sessionUserNameKey)
                UserDefaults.standard.setValue(NSKeyedArchiver.archivedData(withRootObject: session), forKey: Constants.sessionKey)
                UserDefaults.standard.synchronize()
                self.twitterSession = session
                completion(self.session, nil)
            }
            else {
               completion(nil, error)
            }
        }
    }
    
    func restoreSession() -> Bool {
        if let lastSessionData = UserDefaults.standard.object(forKey: Constants.sessionKey) {
            if let lastSession = NSKeyedUnarchiver.unarchiveObject(with: lastSessionData as! Data) {                
                let session = lastSession as! TWTRAuthSession
                self.session = Session(authToken:session.authToken, userID:session.userID)
                self.twitterSession = TWTRSession(authToken: session.authToken, authTokenSecret: session.authTokenSecret, userName: UserDefaults.standard.string(forKey: Constants.sessionUserNameKey)!, userID: session.userID)
            }
        }
        
        return self.session != nil
    }
    
    func logout() {
        Twitter.sharedInstance().sessionStore.logOutUserID((twitterSession?.userID)!)
        session = nil
        twitterSession = nil
        UserDefaults.standard.removeObject(forKey: Constants.sessionKey)
        UserDefaults.standard.removeObject(forKey: Constants.sessionUserNameKey)
    }
    
}
