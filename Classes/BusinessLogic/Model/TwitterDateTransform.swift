//
//  TwitterDateTransform.swift
//  AaaSwift
//
//  Created by Anton Kovalev on 04.11.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import ObjectMapper

class TwitterDateTransform: TransformType {
    typealias Object = Date
    typealias JSON = String
    let formatter : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "eee MMM dd HH:mm:ss ZZZZ yyyy"
        return formatter
    }()
    
    
    func transformFromJSON(_ value: Any?) -> Date? {
        if let value = value {
            return formatter.date(from: value as! String)
        }
        return nil
    }
    
    func transformToJSON(_ value: Date?) -> String? {
        if let value = value {
            return formatter.string(from: value)
        }
        return nil
    }
}
