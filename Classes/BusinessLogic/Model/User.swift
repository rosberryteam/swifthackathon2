//
//  User.swift
//  AaaSwift
//
//  Created by Anton Kovalev on 04.11.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import ObjectMapper

struct User: Mappable {
    
    var id: String?
    var description: String?
    var name: String?
    var imageURL: URL?
    var screenName: String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id              <- map["id_str"]
        description     <- map["description"]
        name            <- map["name"]
        imageURL        <- (map["profile_image_url"], URLTransform())
        screenName      <- map["screen_name"]
    }
    
}
