//
//  Tweet.swift
//  AaaSwift
//
//  Created by Anton Kovalev on 04.11.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import ObjectMapper

struct Tweet: Mappable {
    
    var user: User?
    var date: Date?
    var id: String?
    var text: String?
    var retweetCount: Int?
    var favoriteCount: Int?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        user            <- map["user"]
        date            <- (map["date"], TwitterDateTransform())
        id              <- map["id_str"]
        text            <- map["text"]
        retweetCount    <- map["retweet_count"]
        favoriteCount   <- map["favorite_count"]
    }
    
}
