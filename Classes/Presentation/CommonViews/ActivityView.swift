//
//  ActivityView.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit

class ActivityView: UIView {
    
    static let sharedInstance = ActivityView()
    
    let activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge);
    let activityWindow: UIWindow = UIWindow();
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
        self.addSubview(activityIndicatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        activityIndicatorView.sizeToFit()
        activityIndicatorView.center = CGPoint(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
    }
    
    func show() {
        
        alpha = 0.0
        
        activityWindow.backgroundColor = UIColor.clear
        activityWindow.frame = UIScreen.main.bounds
        activityWindow.addSubview(self)
        
        self.frame = activityWindow.frame
        activityIndicatorView.startAnimating()
        
        activityWindow.makeKeyAndVisible()
        
        UIView.animate(withDuration: 0.25) {
            self.alpha = 1.0
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0.0
            }) { (finished) in
                self.activityWindow.isHidden = true
        }
    }
    
    static func show() {
        self.sharedInstance.show()
    }
    
    static func hide() {
        self.sharedInstance.hide()
    }
}
