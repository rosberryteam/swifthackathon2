//
//  CommentCellItem.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation

struct CommentCellItem {
    let text: String
    let username: String
}
