//
//  DetailsPresenter.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation

class DetailsPresenter {
    
    weak var view: DetailsViewController!
    
    func fetchComments() {
        let commentCellItem = CommentCellItem(text: "123", username: "test1")
        self.view.appendComments(comments: [commentCellItem])
    }
}
