//
//  DetailsViewController.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import URLNavigator
import TableKit
import Framezilla

final class DetailsViewController: UIViewController {
    
    var tweet: Tweet?
    let presenter: DetailsPresenter!
    let tableView = UITableView()
    let label = UILabel()
    var tableDirector: TableDirector?
    
    init() {
        self.presenter = DetailsPresenter()
        super.init(nibName: nil, bundle: nil)
        self.presenter.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
//        self.view.addSubview(tableView)
        self.tableDirector = TableDirector(tableView: tableView)
        
        label.text = "Tweet: \(tweet!.text!)"
        label.numberOfLines = 0
        self.view.addSubview(self.label)
        
        presenter.fetchComments()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.frame
        label.configureFrames { maker in
            maker.edges(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
        }
    }
    
    func appendComments(comments: Array<CommentCellItem>) {
        var rows = Array<TableRow<CommentTableViewCell>>()
        comments.forEach { cellItem in
            let row = TableRow<CommentTableViewCell>(item: cellItem, actions: nil)
            rows.append(row)
        }
        let section = TableSection(rows: rows)
        tableDirector! += section
    }
}

extension DetailsViewController: URLNavigable {
    convenience init?(url: URLConvertible, values: [String: Any]) {
        self.init()
    }
}
