//
//  CommentTableViewCell.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import TableKit
import Framezilla

class CommentTableViewCell: UITableViewCell {
    var label: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label = UILabel()
        self.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        label.configureFrames { maker in
            maker.sizeToFit()
            maker.edges(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
        }
    }
}

extension CommentTableViewCell: ConfigurableCell {
    func configure(with cellItem: CommentCellItem) {
        label.text = cellItem.text
    }
}
