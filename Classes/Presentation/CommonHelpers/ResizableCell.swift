//
//  ResizableCell.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit

public protocol ResizableCell {
    
    func expectedHeight(constrainedToWidth: CGFloat) -> CGFloat
}
