//
//  TableViewCellHeightCalculator.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import TableKit

open class TableViewCellHeightCalculator<CellType: ResizableCell>: RowHeightCalculator where CellType: UITableViewCell {
    
    private(set) weak var tableView: UITableView?
    
    public init(tableView: UITableView?) {
        self.tableView = tableView
    }
    
    public func height(forRow row: Row, at indexPath: IndexPath) -> CGFloat {
        let cell: CellType = tableView?.dequeueReusableCell(withIdentifier: row.reuseIdentifier) as! CellType
        row.configure(cell)
        return CGFloat(heightForCell(cell: cell))
    }
    
    func heightForCell(cell: CellType) -> CGFloat {
        return cell.expectedHeight(constrainedToWidth: tableView!.bounds.size.width)
    }
    
    public func estimatedHeight(forRow row: Row, at indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    public func invalidate() {
        
    }
}
