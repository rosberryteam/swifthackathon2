//
//  FeedPresenter.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation
import URLNavigator

class FeedPresenter {
    
    weak var view: FeedViewController!
    let timelineService = TimelineService()
    let authorizationService = AuthorizationService.sharedInstance
    
    
    func fetchData() {
        timelineService.timeline { (tweets, error) in
            if let tweets = tweets {
                self.view.setTweets(tweets)
            } else {
                self.view.failLoading()
                Navigator.open("app://alert?title=Error&message=Error loading tweets")
            }
        }
    }
    
    func fetchMoreData() {
        timelineService.timeline { (tweets, error) in
            if let tweets = tweets {
                self.view.appendTweets(tweets)
            }
        }
    }
    
    func showDetailsFor(tweet: Tweet) {
        let details = Navigator.push("app://details") as! DetailsViewController
        details.tweet = tweet
    }
    
    func logout() {
        authorizationService.logout()
    }
    
}
