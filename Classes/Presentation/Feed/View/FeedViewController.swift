//
//  FeedViewController.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import URLNavigator
import TableKit
import PullToRefresh

final class FeedViewController: UIViewController {
    
    let presenter: FeedPresenter!
    
    var tableView: UITableView!
    var tableDirector: TableDirector!
    var refreshControl: PullToRefresh!
    
    
    init() {
        self.presenter = FeedPresenter()
        super.init(nibName: nil, bundle: nil)
        self.presenter.view = self
    }
    
    deinit {
        tableView.removePullToRefresh(tableView.topPullToRefresh!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        tableView = UITableView()
        view.addSubview(tableView)
        
        tableDirector = TableDirector(tableView: tableView)
        tableDirector.shouldUsePrototypeCellHeightCalculation = false
        tableDirector.rowHeightCalculator = TableViewCellHeightCalculator<FeedTableViewCell>(tableView: tableView);
        
        navigationItem.title = "Feed"
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(logout))
        
        presenter.fetchData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if refreshControl == nil {
            let refreshControl = PullToRefresh()
            tableView.addPullToRefresh(refreshControl) {
                self.presenter.fetchData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        tableView.frame = view.frame;
    }
    
    func section(with tweets: Array<Tweet>) -> TableSection {
        let action = TableRowAction<FeedTableViewCell>(.click, handler: {[weak self] (options: TableRowActionOptions<FeedTableViewCell>) in
            let index = options.indexPath.row
            self?.presenter.showDetailsFor(tweet: tweets[index])
            })
        
        var rows = Array<TableRow<FeedTableViewCell>>()
        for tweet in tweets {
            let row = TableRow<FeedTableViewCell>(item: tweet, actions: [action])
            rows.append(row)
        }
        
        return TableSection(rows: rows)
        
    }
    
    public func setTweets(_ tweets: Array<Tweet>) {
        tableView.endRefreshing(at: .top)
        
        tableDirector.clear()
        tableDirector += section(with: tweets)
        tableDirector.reload()
    }
    
    public func appendTweets(_ tweets: Array<Tweet>) {
        tableDirector += section(with: tweets)
        tableDirector.reload()
    }
    
    public func failLoading() {
        tableView.endRefreshing(at: .top)
    }

    func logout() {
        presenter.logout()
        _ = navigationController?.popViewController(animated: true)
    }
}

extension FeedViewController: URLNavigable {
    convenience init?(url: URLConvertible, values: [String: Any]) {
        self.init()
    }
}
