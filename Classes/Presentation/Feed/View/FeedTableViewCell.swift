//
//  FeedTableViewCell.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import TableKit
import Framezilla
import Kingfisher

class FeedTableViewCell: UITableViewCell {
    var tweetImageView: UIImageView!
    var label: UILabel!
    var usernameLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        self.tweetImageView = UIImageView()
        self.tweetImageView.backgroundColor = UIColor.blue
        self.addSubview(self.tweetImageView)
        
        self.label = UILabel()
        self.label.font = UIFont.systemFont(ofSize: 14.0)
        self.label.numberOfLines = 0
        self.addSubview(self.label)
        
        self.usernameLabel = UILabel()
        self.usernameLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.usernameLabel.numberOfLines = 1
        self.addSubview(self.usernameLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.tweetImageView.configureFrames { maker in
            maker.top(inset: 8.0).left(inset: 8.0)
            maker.width(40.0)
            maker.height(40.0)
        }
        
        self.label.configureFrames { maker in
            maker.sizeToFit()
            maker.top(inset: 30.0).right(inset: 8.0).bottom(inset: 8.0)
            maker.left(to: self.tweetImageView.nui_right, inset: 8.0)
        }
        
        self.usernameLabel.configureFrames { maker in
            maker.top(inset: 8.0).right(inset: 8.0)
            maker.left(to: self.tweetImageView.nui_right, inset: 8.0)
            maker.bottom(to: self.label.nui_top, inset: 4.0)
        }
    }
    
}

extension FeedTableViewCell: ConfigurableCell {
    
    func configure(with tweet: Tweet) {
        label.text = tweet.text
        usernameLabel.text = tweet.user?.name
        
        tweetImageView.kf.cancelDownloadTask()
        tweetImageView.kf.setImage(with: tweet.user?.imageURL)
    }
    
}

extension FeedTableViewCell: ResizableCell {
    
    func expectedHeight(constrainedToWidth: CGFloat) -> CGFloat {
    
        let size = CGSize(width: constrainedToWidth - 56.0, height: 0.0)
        let targetSize = label.sizeThatFits(size)
        return max(targetSize.height + 40.0, 56.0)
        
        /*
        let attributedString = NSAttributedString.init(string: label.text!, attributes: attributes)
        let size = CGSize(width: constrainedToWidth, height: 0.0)
        let rect = attributedString.boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
        return rect.size.height
        */
    }
    
}
