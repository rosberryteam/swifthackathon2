//
//  Routing.swift
//  AaaSwift
//
//  Created by Anton K on 11/4/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import URLNavigator

struct Routing {
    
    static func initialize() {
        Navigator.map("app://alert", self.alert)
        Navigator.map("app://feed", FeedViewController.self)
        Navigator.map("app://details", DetailsViewController.self)
    }
    
    private static func alert(URL: URLConvertible, values: [String: Any]) -> Bool {
        let title = URL.queryParameters["title"]
        let message = URL.queryParameters["message"]
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        Navigator.present(alertController)
        return true
    }
    
}

