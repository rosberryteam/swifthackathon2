 //
//  LoginViewController.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit
import URLNavigator
import Framezilla

class LoginViewController: UIViewController {
    let loginButton = UIButton()
    let presenter: LoginPresenter!
    let imageView = UIImageView()
    
    
    init() {
        self.presenter = LoginPresenter()
        super.init(nibName: nil, bundle: nil)
        self.presenter.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Welcome"
        
        loginButton.backgroundColor = UIColor(red: 37.0/255.0, green: 175.0/255.0, blue: 234.0/255.0, alpha: 1.0)
        loginButton.center = self.view.center
        loginButton.layer.cornerRadius = 2
        loginButton.setTitle("Login", for: .normal)
        loginButton.addTarget(self, action: #selector(LoginViewController.login(sender:)), for: .touchUpInside)
        view.addSubview(self.loginButton)
        
        imageView.image = #imageLiteral(resourceName: "bird")
        view.addSubview(imageView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.tryAutoLogin()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imageView.configureFrames { (maker) in
            maker.sizeToFit()
            maker.centerX(to: self.view.nui_centerX, offset: 0.0)
            maker.centerY(to: self.view.nui_centerY, offset: 50.0)
        }
        
        loginButton.configureFrames { (maker) in
            maker.width(200.0).height(44.0)
            maker.top(to: self.imageView.nui_bottom, inset: 16.0)
            maker.centerX(to: self.view.nui_centerX, offset: 0.0)
        }
    }
    
    func login(sender: AnyObject) {
        ActivityView.sharedInstance.show()
        presenter.logIn { error in
            ActivityView.sharedInstance.hide()
            if let returnedError = error {
                print(returnedError.localizedDescription)
            }
            else {
                print("Success")
            }
        }
    }
}
