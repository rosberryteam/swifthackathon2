//
//  LoginPresenter.swift
//  AaaSwift
//
//  Created by Artem Novichkov on 04/11/2016.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import UIKit.UIViewController
import URLNavigator

class LoginPresenter {
    weak var view: UIViewController!
    let authorizationService = AuthorizationService.sharedInstance
    
    
    func logIn(completion: @escaping (Error?) -> Swift.Void) {
        authorizationService.logIn() { session, error in
            completion(error);
            Navigator.push("app://feed")
        }
    }
    
    func tryAutoLogin() {
        if authorizationService.restoreSession() {
            Navigator.push("app://feed")
        }
    }
    
}
